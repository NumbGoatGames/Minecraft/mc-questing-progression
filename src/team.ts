import { read, getKey } from "./read_nbt";
import * as _ from "lodash";

export class Team {
    id: string;
    name: string;
    canJoin: boolean;

    constructor() {}

    static async read(path: string, name: string): Promise<Team> {
        let nbtTeam = await read(`${path}/${name}`);

        let team = new Team();
        team.id = getKey(nbtTeam, 'Color');
        team.name = getKey(nbtTeam, 'Title');
        team.canJoin = getKey(nbtTeam, 'FreeToJoin') == 0;
        
        return team;
    }

    static async readAll(path: string): Promise<Team[]> {
        let files = ['blue.dat', 'green.dat', 'red.dat', 'yellow.dat'];
        let teams: Team[] = [];
        for(let file of files) {
            let team = await this.read(path, file);
            teams.push(team);
        }
        return teams;
    }
}