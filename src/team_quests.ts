import * as fspath from 'path';
import { read, getKey } from './read_nbt';
import * as fs from 'fs';
import * as _ from 'lodash';


export class TeamQuest {
    team_id: string;
    claimed_rewards: any[] = [];
    tasks: Task[] = [];

    constructor() {}

    static async read(path: string, name: string): Promise<TeamQuest> {
        let nbtTeam = await read(`${path}/${name}`);
        let teamQuest = new TeamQuest();
        teamQuest.claimed_rewards = getKey(nbtTeam, 'ClaimedPlayerRewards');
        teamQuest.tasks = _.flatMap(getKey(nbtTeam, 'TaskData'), chapter_task => {
            return _.map(chapter_task['val'], (taskNbt) => {
                let task = new Task();
                teamQuest.team_id = _.replace(name, '.ftbquests.dat', '');
                task.chapter_id = chapter_task['name'];
                task.name = taskNbt['name'];
                task.complete = getKey(taskNbt, taskNbt['name'], 0) == 1;

                return task; 
            });
        });
        
        return teamQuest;
    }

    static async readAll(path: string): Promise<TeamQuest[]> {
        let files = ['blue.ftbquests.dat', 'green.ftbquests.dat', 'red.ftbquests.dat', 'yellow.ftbquests.dat'];
        let teamQuests: TeamQuest[] = [];
        for(let file of files) {
            let filePath = fspath.join(path, file)
            if(fs.existsSync(filePath)) {
                let teamQuest = await this.read(path, file);
                teamQuests.push(teamQuest);
            }
        }
        return teamQuests;
    }
}

export class Task {
    chapter_id: string;
    name: string;
    complete: boolean;
}