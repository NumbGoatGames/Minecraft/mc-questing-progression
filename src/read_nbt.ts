import { NbtReader } from 'node-nbt';
import * as fs from 'fs';
import * as zlib from 'zlib';
import * as _ from "lodash";

export function read(path: string) {
    return new Promise((resolve, reject) => {
        fs.readFile(path, (err, data) => {
            if(err) {
                return reject(err);
            }
            zlib.gunzip(data, (err, buffer) => {
                if (!err) {
                    var d = NbtReader.readTag(buffer);
                    d = NbtReader.removeBufferKey(d);
                    return resolve(d);
                } else {
                    return reject(err);
                }
            });
        });
    });
}

export function getKey(nbt: any, key: string, def?: any) {
    return _.get(_.find(nbt['val'], v => v.name == key), ['val'], def);
}

export function getList(nbtList: any) {
    return _.get(nbtList, ['list'], []);
}