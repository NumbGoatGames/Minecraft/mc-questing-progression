import { read, getKey, getList } from "./read_nbt";
import {Quest} from "./quests";
import * as fs from "fs";
import * as path from "path";

export class QuestBook {
    public entries: Chapter[] = [];
    constructor() {}

    /**
     * Read all of the chapter components from a directory.
     * 
     * @param path The base dir of this chapter.
     */
    public static async read(questsPath: string): Promise<QuestBook> {
        if(!fs.existsSync(questsPath)){
            throw new Error(`Could not find the chapter directory: ${path}`);
        }

        let qb = new QuestBook();
        let directories = [];

        for(let child of fs.readdirSync(questsPath)){
            if(fs.lstatSync(child).isDirectory){
                directories.push(child);
            }
        }        

        let chapters: Chapter[] = [];

        for(let chapterDir of directories){
            let chapter = await this.readChapterDir(path.join(questsPath,chapterDir));
            if(chapter != null){
                chapters.push(chapter);
            }
        }

        qb.entries = chapters;

        return qb;
    }

    private static async readChapterDir(chapterPath: string): Promise<Chapter>{
        let entry = new Chapter();

        let chapterIndexFile = path.join(chapterPath,"chapter.nbt");

        if(!fs.existsSync(chapterIndexFile)){
            // This is not a quest chapter directory, ignore.
            return null;
        }

        let chapterIndexNbt = await read(chapterIndexFile);
        let id = path.dirname(chapterPath);
        let title = getKey(chapterIndexNbt,"title");
        entry.id = id;
        entry.title = title;

        let files: string[] = fs.readdirSync(chapterPath);

        for(let file of files){
            if(file == "chapter.nbt"){
                continue;
            }
            let nbt = read(path.join(chapterPath,""))
            let entry = new Quest();
            let entryTitle = getKey()
        }
        
        return entry;
    }

    // public static async old_read(path: string): Promise<QuestBook> {
    //     if(!fs.existsSync(path)){
    //         throw new Error(`Could not find the quests file ${path}`);
    //     }
    //     let chapter = new QuestBook();

    //     let fileNbt = await read(path);
    //     let chapterNbt = getKey(fileNbt, "chapters");

    //     let entries = getList(chapterNbt);
    //     for(let nbtEntry of entries) {
    //         let entry = new QuestBook();
    //         entry.id = getKey(nbtEntry, 'id')
    //         entry.title = getKey(nbtEntry, 'title')
    //         //entry.icon = getKey(nbtEntry, 'icon')
    //         entry.quests = [];
            
    //         let questsNbt = getKey(nbtEntry, 'quests');

    //         for(let questNbt of getList(questsNbt)) {
    //             let quest = new Quest();

    //             quest.id = getKey(questNbt,"id");
    //             quest.title = getKey(questNbt,"title");
    
    //             entry.quests.push(quest);
    //         }

    //         chapter.entries.push(entry);
    //     }

    //     return chapter;
    // }
}

export class Chapter {
    id: string;
    icon: string;
    title: string;
    quests: Quest[];

}