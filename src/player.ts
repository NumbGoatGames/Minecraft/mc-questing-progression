import { read, getKey } from "./read_nbt";
import * as fs from 'fs';
import * as _ from "lodash";

export class Player {
    id: string;
    name: string;
    team_id: string;
    last_seen: Date;

    constructor() {}

    static async read(path: string, name: string): Promise<Player> {
        let nbtPlayer = await read(`${path}/${name}`);

        let player = new Player();
        player.id = getKey(nbtPlayer, 'UUID');
        player.name = getKey(nbtPlayer, 'Name');
        player.team_id = getKey(nbtPlayer, 'TeamID');
        player.last_seen = new Date(new Date().getTime() - getKey(nbtPlayer, 'LastTimeSeen'));
        
        return player;
    }

    static async readAll(path: string): Promise<Player[]> {
        let files = fs.readdirSync(path);
        let players: Player[] = [];
        for(let file of files) {
            let player = await this.read(path, file);
            players.push(player);
        }
        return players;
    }
}