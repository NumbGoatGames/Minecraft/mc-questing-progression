import * as path from 'path';
import { Player } from './player';
import { Team } from './team';
import { TeamQuest } from './team_quests';
import { QuestBook } from "./chapter";
import * as _ from 'lodash';

export async function jsonData(config: { root: string, players: string, teams: string, quests: string }) {
  let playerFolder = path.join(config.root, config.players);
  let teamsFolder = path.join(config.root, config.teams);
  let questFile = path.join(config.root, config.quests);

  let players = await Player.readAll(playerFolder);
  let teams = await Team.readAll(teamsFolder);
  let teamQuests = await TeamQuest.readAll(teamsFolder);
  let chapter = await QuestBook.read(questFile);

  let teamStatuses = [];

  for (let entry of chapter.entries) {
    let chapterStatus = {
      entry_id: entry.id,
      entry: entry.title,
      quests: _.map(entry.quests, q => {
        let questProgress = {
          quest_id: q.id,
          quest: q.title,
          results: []
        };

        for (let team of teams) {
          let quests = _.find(teamQuests, t => t.team_id == team.id);
          questProgress.results.push({
            team_id: team.id,
            team_name: team.name,
            complete: quests ? _.get(_.find(quests.tasks, t => t.chapter_id == entry.id && t.name == q.id), 'complete', false) : false
          });
        }

        return questProgress;
      })
    };
    teamStatuses.push(chapterStatus);
  }

  return {
    players: players,
    teams: teams,
    chapter: chapter,
    statuses: teamStatuses
  };
}
